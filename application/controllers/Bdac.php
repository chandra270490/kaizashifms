<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Bdac extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('bdam');
	}
	
	//Reports
	public function index(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Business Associate Dashboard' => 'bdac',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/bda/bda_db', $data); 
		$this->load->view('admin/footer');
	}

	/*************************************/
	/***Business Associate***/
	/*************************************/

	//Business Associate List
	public function bda_list(){ 
		$tbl_nm = "bda_mst";
		$data = array();
		$data['list_title'] = "Businees Associate List";
		$data['list_url'] = "bdac/bda_list";
		$data['tbl_nm'] = "bda_mst";
		$data['primary_col'] = "bda_id";
		$data['edit_url'] = "bdac/bda_add";
		$data['edit_enable'] = "yes";

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Business Associate Dashboard' => 'bdac',
			'Business Associate List' => 'bdac/bda_list',
		);

		$data['ViewHead'] = $this->bdam->ListHead($tbl_nm);
		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data);
		$this->load->view('admin/footer');
	}

	//Business Associate Add
	public function bda_add(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Business Associate Dashboard' => 'bdac',
			'Business Associate List' => 'bdac/bda_list',
			'Business Associate Add' => 'bdac/bda_add',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/bda/bda_add', $data); 
		$this->load->view('admin/footer');
	}

	//Business Associate Query
	public function bda_entry(){
		$data = array();
		$data['bda_entry'] = $this->bdam->bda_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'bdac';
		$this->load->view('admin/QueryPage',$data); 
	}

	/*************************************/
	/***Commission Agent***/
	/*************************************/

	//Commission Agent List
	public function comagnt_list(){
		$tbl_nm = "comagnt_mst";
		$data = array();
		$data['list_title'] = "Commission Agent Master List";
		$data['list_url'] = "bdac/comagnt_list";
		$data['tbl_nm'] = "comagnt_mst";
		$data['primary_col'] = "comagnt_id";
		$data['edit_url'] = "bdac/comagnt_add";
		$data['edit_enable'] = "yes";
		$data['ViewHead'] = $this->bdam->ListHead($tbl_nm);

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Business Associate Dashboard' => 'bdac',
			'Commission Agent List' => 'bdac/comagnt_list',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data);
		$this->load->view('admin/footer');
	}

	//Commission Agent Add
	public function comagnt_add(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Business Associate Dashboard' => 'bdac',
			'Commission Agent List' => 'bdac/comagnt_list',
			'Commission Agent Add' => 'bdac/comagnt_add',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/bda/comagnt_add', $data); 
		$this->load->view('admin/footer');
	}

	//Commission Agent Query
	public function comagnt_entry(){ 
		
		$data = array();
		$data['bda_entry'] = $this->bdam->comagnt_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'bdac';
		$this->load->view('admin/QueryPage',$data); 
	}
}
