<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Reportsc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('reportsm');
	}

	/*************************************/
	/***Reports***/
    /*************************************/
    public function index(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/reports_db', $data); 
		$this->load->view('admin/footer');
    }
	
	//master franchisee
	public function customers(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
			'Customers List' => 'reportsc/customers',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/customers', $data); 
		$this->load->view('admin/footer');
    }
    
    //master franchisee
	public function franchisee_inquiry(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
			'Franchisee Inquiries' => 'reportsc/franchisee_inquiry',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/franchisee_inquiry', $data); 
		$this->load->view('admin/footer');
    }
    
    //master franchisee
	public function franchisee_inprocess(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
			'Franchise In-Process' => 'reportsc/franchisee_inprocess',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/franchisee_inprocess', $data); 
		$this->load->view('admin/footer');
    }
    
    //master franchisee
	public function franchisee_inline(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
			'Franchise Inline' => 'reportsc/franchisee_inline',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/franchisee_inline', $data); 
		$this->load->view('admin/footer');
    }
    
    //master franchisee
	public function master_franchisee(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
			'Master Franchisee' => 'reportsc/master_franchisee',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/master_franchisee', $data); 
		$this->load->view('admin/footer');
    }
    
    //master franchisee
	public function online_orders(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
			'Online Orders' => 'reportsc/online_orders',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/online_orders', $data); 
		$this->load->view('admin/footer');
    }
    
    //master franchisee
	public function pending_po(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
			'Pending Purchase Orders' => 'reportsc/pending_po',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/pending_po', $data); 
		$this->load->view('admin/footer');
    }
    
    //master franchisee
	public function products(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
			'Products List' => 'reportsc/products',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/products', $data); 
		$this->load->view('admin/footer');
    }
    
    //master franchisee
	public function sales(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
			'Sales Report' => 'reportsc/sales',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/sales', $data); 
		$this->load->view('admin/footer');
	}

	//Statisticks Update Module
	public function stat_updt(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Reports Dashboard' => 'reportsc',
			'Statistics Update' => 'reportsc/stat_updt',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/reports/stat_updt', $data); 
		$this->load->view('admin/footer');
	}

	public function stat_updt_entry(){ 
		$data = array();
		$data['stat_updt_entry'] = $this->reportsm->stat_updt_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'reportsc';
		$this->load->view('admin/QueryPage',$data); 
	}
	


}
