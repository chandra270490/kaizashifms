<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Productsc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('productsm');
	}

	/*************************************/
	/***Products Add***/
	/*************************************/
	
	//Dashboard Products
	public function index(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Products Dashboard' => 'productsc',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/products/products_db', $data); 
		$this->load->view('admin/footer');
	}

	//Products List
	public function products_list(){ 
		$tbl_nm = "products_mst";
		$data = array();
		$data['list_title'] = "Products List";
		$data['list_url'] = "productsc/products_list";
		$data['tbl_nm'] = "products_mst";
		$data['primary_col'] = "prod_id";
		$data['edit_url'] = "productsc/products_add";
		$data['edit_enable'] = "yes";

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Products Dashboard' => 'productsc',
			'Products List' => 'productsc/products_list',
		);

		$data['ViewHead'] = $this->productsm->ListHead($tbl_nm);
		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data);
		$this->load->view('admin/footer');
	}

	//Products Add
	public function products_add(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Products Dashboard' => 'productsc',
			'Products List' => 'productsc/products_list',
			'Products Add' => 'productsc/products_add',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/products/products_add', $data); 
		$this->load->view('admin/footer');
	}

	//Products Query
	public function products_entry(){ 
		$data = array();
		$data['products_entry'] = $this->productsm->products_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'productsc';
		$this->load->view('admin/QueryPage',$data); 
	}

	//Products CSV Import
	public function products_upload()
	{ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Products Dashboard' => 'productsc',
			'Products List' => 'productsc/products_list',
			'Products Upload' => 'productsc/products_upload',
		);
		
		$this->load->view('admin/header');
		$this->load->view('admin/modules/products/import_data', $data);
		$this->load->view('admin/footer');
		
		if(isset($_POST["submit"]))
		{
			$file = $_FILES['file']['tmp_name'];
			$handle = fopen($file, "r");
			$c = 0;//
			while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
			{
				$fname = $filesop[0];
				$lname = $filesop[1];
				if($c<>0){					//SKIP THE FIRST ROW
					$this->productsm->saverecords($fname,$lname);
				}
				$c = $c + 1;
			}
			//echo "sucessfully import data !";
				
		}
	}
}
