<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Saic extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('saim');
		 $this->load->library('excel');
	}
	
	//SAI Master Dashboard
	public function index(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Shri Agro Industries' => 'saic',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sai/sai_db', $data); 
		$this->load->view('admin/footer');
    }
    
    //Masters
	public function masters(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Masters' => 'saic/masters',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/sai/masters_db', $data); 
		$this->load->view('admin/footer');
    }
    
    //Vendor List
    public function vendor_list(){ 
        $tbl_nm = "vendor_mst";
		$data = array();
		$data['list_title'] = "Vendor List";
		$data['list_url'] = "saic/vendor_list";
		$data['tbl_nm'] = "vendor_mst";
		$data['primary_col'] = "vendor_id";
		$data['edit_url'] = "saic/vendor_add";
		$data['edit_enable'] = "No";

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Masters' => 'saic/masters',
            'Vendor List' => 'saic/vendor_list',
		);

		$data['ViewHead'] = $this->saim->ListHead($tbl_nm);
		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data);
		$this->load->view('admin/footer');
    }

    //Vendor Add
    public function vendor_add(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Masters' => 'saic/masters',
            'Vendor List' => 'saic/vendor_list',
            'Vendor Add' => 'saic/vendor_add',
        );
        
		$this->load->view('admin/header');
		$this->load->view('admin/modules/sai/vendor_add', $data);
		$this->load->view('admin/footer');
    }

    //Vendor Query
	public function vendor_entry(){ 
		$data = array();
		$data['vendor_entry'] = $this->saim->vendor_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'saic/vendor_list';
		$this->load->view('admin/QueryPage',$data); 
    }
    
    //GST List
    public function gst_list(){ 
        $tbl_nm = "gst_mst";
		$data = array();
		$data['list_title'] = "GST List";
		$data['list_url'] = "saic/gst_list";
		$data['tbl_nm'] = "gst_mst";
		$data['primary_col'] = "gst_id";
		$data['edit_url'] = "saic/gst_add";
		$data['edit_enable'] = "No";

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Masters' => 'saic/masters',
            'GST List' => 'saic/gst_list',
		);

		$data['ViewHead'] = $this->saim->ListHead($tbl_nm);
		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data);
		$this->load->view('admin/footer');
    }

    //GST Add
    public function gst_add(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Masters' => 'saic/masters',
            'GST List' => 'saic/gst_list',
            'GST Add' => 'saic/gst_add',
        );
        
		$this->load->view('admin/header');
		$this->load->view('admin/modules/sai/gst_add', $data);
		$this->load->view('admin/footer');
    }

    //GST Query
	public function gst_entry(){ 
		$data = array();
		$data['gst_entry'] = $this->saim->gst_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'saic/gst_list';
		$this->load->view('admin/QueryPage',$data); 
	}
	
	//Category List
    public function category_list(){ 
        $tbl_nm = "category_mst";
		$data = array();
		$data['list_title'] = "Category List";
		$data['list_url'] = "saic/category_list";
		$data['tbl_nm'] = "category_mst";
		$data['primary_col'] = "category_id";
		$data['edit_url'] = "saic/category_add";
		$data['edit_enable'] = "No";

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Masters' => 'saic/masters',
            'Category List' => 'saic/category_list',
		);

		$data['ViewHead'] = $this->saim->ListHead($tbl_nm);
		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data);
		$this->load->view('admin/footer');
    }

    //Category Add
    public function category_add(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Masters' => 'saic/masters',
            'Category List' => 'saic/category_list',
            'Category Add' => 'saic/category_add',
        );
        
		$this->load->view('admin/header');
		$this->load->view('admin/modules/sai/category_add', $data);
		$this->load->view('admin/footer');
    }

    //Category Query
	public function category_entry(){ 
		$data = array();
		$data['category_entry'] = $this->saim->category_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'saic/category_list';
		$this->load->view('admin/QueryPage',$data); 
    }
    
    //Raw Material List
    public function raw_mat_list(){ 
        $tbl_nm = "raw_mat_mst";
		$data = array();
		$data['list_title'] = "Raw Material List";
		$data['list_url'] = "saic/raw_mat_list";
		$data['tbl_nm'] = "raw_mat_mst";
		$data['primary_col'] = "product_id";
		$data['edit_url'] = "saic/raw_mat_add";
		$data['edit_enable'] = "No";

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Masters' => 'saic/masters',
            'Raw Material List' => 'saic/raw_mat_list',
		);

		$data['ViewHead'] = $this->saim->ListHead($tbl_nm);
		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data);
		$this->load->view('admin/footer');
    }

    //Raw Material Add
    public function raw_mat_add(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Masters' => 'saic/masters',
            'Raw Material List' => 'saic/raw_mat_list',
            'Raw Material Add' => 'saic/raw_mat_add',
        );
        
		$this->load->view('admin/header');
		$this->load->view('admin/modules/sai/raw_mat_add', $data);
		$this->load->view('admin/footer');
    }

    //Raw Material Query
	public function raw_mat_entry(){ 
		$data = array();
		$data['raw_mat_entry'] = $this->saim->raw_mat_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'saic/raw_mat_list';
		$this->load->view('admin/QueryPage',$data); 
    }
    
    //Bill List
    public function bill_list(){ 
        $tbl_nm = "bill_mst";
		$data = array();
		$data['list_title'] = "Raw Material Bill List";
		$data['list_url'] = "saic/bill_list";
		$data['tbl_nm'] = "bill_mst";
		$data['primary_col'] = "bill_mst_id";
		$data['edit_url'] = "saic/bill_add";
		$data['edit_enable'] = "No";

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Raw Material Bill List' => 'saic/bill_list',
		);

		//Table Header Array
		$data['table_hdr'] =
		array(
			'Bill Id' => 'bill_mst_id',
			'Vendor Id' => 'vendor_id',
			'Vendor Name' => 'vendor_name',
			'Invoice No' => 'invoice_no',
			'Invoice Date' => 'invoice_date',
			'Total Qty' => 'tot_qty',
			'Total Amt' => 'tot_amt',
			'Created By' => 'created_by',
			'Created Date' => 'created_date',
			'Modified By' => 'modified_by',
			'Modified Date' => 'modified_date',
		);

		$data['ViewHead'] = $this->saim->ListHead($tbl_nm);
		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data);
		$this->load->view('admin/footer');
    }

    //Bill Add
    public function bill_add(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
            'Shri Agro Industries' => 'saic',
            'Masters' => 'saic/masters',
            'Raw Material Bill List' => 'saic/bill_list',
            'Raw Material Bill Add' => 'saic/bill_list',
        );
        
		$this->load->view('admin/header');
		$this->load->view('admin/modules/sai/bill_add', $data);
		$this->load->view('admin/footer');
	}
	
	//Bill Add Ajax
    public function bill_add_ajax(){ 
		$this->load->view('admin/modules/sai/bill_add_ajax');
    }

    //Bill Query
	public function bill_entry(){ 
		$data = array();
		$data['bill_entry'] = $this->saim->bill_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'saic/bill_list';
		$this->load->view('admin/QueryPage',$data); 
	}

	
}
