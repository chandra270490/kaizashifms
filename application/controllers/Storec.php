<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Storec extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('storem');
	}
	
	//Reports
	public function index(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Store Dashboard' => 'storec',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/store/store_db', $data); 
		$this->load->view('admin/footer');
	}

	/*************************************/
	/***Store***/
	/*************************************/

	//Store List
	public function store_list(){ 
		$tbl_nm = "store_mst";
		$data = array();
		$data['list_title'] = "Store List";
		$data['list_url'] = "storec/store_list";
		$data['tbl_nm'] = "store_mst";
		$data['primary_col'] = "store_no";
		$data['edit_url'] = "storec/store_add";
		$data['edit_enable'] = "yes";

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Store Dashboard' => 'storec',
			'Store List' => 'storec/store_list',
		);

		$data['ViewHead'] = $this->storem->ListHead($tbl_nm);
		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data);
		$this->load->view('admin/footer');
	}

	//Store Add
	public function store_add(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Store Dashboard' => 'storec',
			'Store List' => 'storec/store_list',
			'Store Add' => 'storec/store_add?id=',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/store/store_add', $data); 
		$this->load->view('admin/footer');
	}

	//Store Query
	public function store_entry(){ 
		$data = array();
		$data['company_entry'] = $this->storem->store_entry($data);
		$data['message'] = 'Data Inserted Successfully';

		$data['url'] = 'storec';
		$this->load->view('admin/QueryPage',$data); 
	}
}
