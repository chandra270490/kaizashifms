<?php $this->load->helper('masterdb'); ?>
<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i> Master Dashboard</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_list">
        <div class="info-box green-bg">
          <div class="count"><?php echo db_value('franchisee_mst'); ?></div>
          <div class="title">Franchisee</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/reportsc/master_franchisee">
        <div class="info-box green-bg">
          <div class="count"><?php echo db_mst_frac('franchisee_mst'); ?></div>
          <div class="title">Master Franchisee</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/storec/store_list">
        <div class="info-box green-bg">
          <div class="count"><?php echo db_store(); ?></div>
          <div class="title">Stores</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/reportsc/products">
        <div class="info-box green-bg">
          <div class="count"><?php echo db_products(); ?></div>
          <div class="title">Products</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/reportsc/sales">
        <div class="info-box green-bg">
          <div class="count"><?php echo tot_sales(); ?></div>
          <div class="title">Total Sales</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/reportsc/customers">
        <div class="info-box green-bg">
          <div class="count"><?php echo tot_cust(); ?></div>
          <div class="title">Customers</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

    </div>
    <!--/.row-->

    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/bdac/bda_list">
        <div class="info-box green-bg">
          <div class="count"><?php echo db_value('bda_mst'); ?></div>
          <div class="title">Business Associates</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_steps">
        <div class="info-box green-bg">
          <div class="count"><?php echo fran_inq('franchisee_inq_mst'); ?></div>
          <div class="title">Franchise Inquiry</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_steps">
        <div class="info-box green-bg">
          <div class="count"><?php echo fran_inq('franchisee_inq_mst'); ?></div>
          <div class="title">Franchise In-Line</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_steps">
        <div class="info-box green-bg">
          <div class="count"><?php echo fran_inq('franchisee_inq_mst'); ?></div>
          <div class="title">Franchise In Process</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/reportsc/online_orders">
        <div class="info-box green-bg">
          <div class="count"><?php echo oos_value('products_mst'); ?></div>
          <div class="title">Online Orders</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/reportsc/pending_po">
        <div class="info-box green-bg">
          <div class="count">0</div>
          <div class="title">Pending PO</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

    </div>
    <!--/.row-->
    <!--
    <div class="panel-body">
      <div class="tab-pane" id="chartjs">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">Total Sales</header>
                    <div class="panel-body text-center">
                        <canvas id="bar" height="300" width="500"></canvas>
                    </div>
                </section>
            </div>
        </div>
      </div>
    </div>
    -->

    <!--<div id="chartContainer" style="height: 370px; width: 100%;"></div><br><br>-->
    <!--
    <div class="row" style="text-align:center">
        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/franchiseesc">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/inventory_management.png" width="50%"/><br /><br />
                Franchisees
            </a>
        </div>

        <div class="col-lg-2">
          <a href="<?php echo base_url(); ?>index.php/storec">
            <img src="<?php echo base_url(); ?>assets/admin/db/updated/hrms.png" width="50%"/><br /><br />
            Store
          </a>
        </div>

    	  <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/productsc">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Products
            </a>
        </div>

        <div class="col-lg-2">
          <a href="<?php echo base_url(); ?>index.php/purchasec">
            <img src="<?php echo base_url(); ?>assets/admin/db/updated/sales_management.png" width="50%"/><br /><br />
            Purchase
          </a>
        </div>

        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/warehousec">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/logistics_management.png" width="50%"/><br /><br />
                Warehouse
            </a>
        </div>

        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/bdac">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/logistics_management.png" width="50%"/><br /><br />
                Business Associate
            </a>
        </div>
    </div><br /><br />

    <div class="row" style="text-align:center">
        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/settingsc">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/logistics_management.png" width="50%"/><br /><br />
                Settings
            </a>
        </div>
    </div><br /><br />
    -->
    
  </section>
</section>
 
  
<script>
//knob
$(function() {
  $(".knob").knob({
    'draw' : function () { 
      $(this.i).val(this.cv + '%')
    }
  })
});

//carousel
$(document).ready(function() {
    $("#owl-slider").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem : true

    });
});

//custom select box
$(function(){
    $('select.styled').customSelect();
});
	  
/* ---------- Map ---------- */
$(function(){
  $('#map').vectorMap({
    map: 'world_mill_en',
    series: {
      regions: [{
        values: gdpData,
        scale: ['#000', '#000'],
        normalizeFunction: 'polynomial'
      }]
    },
  backgroundColor: '#eef3f7',
    onLabelShow: function(e, el, code){
      el.html(el.html()+' (GDP - '+gdpData[code]+')');
    }
  });
});

</script>
<!--
<script>
window.onload = function() {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	title: {
		text: "Total Sales"
	},
	data: [{
		type: "bar",
		startAngle: 240,
		yValueFormatString: "##0.00\"%\"",
		indexLabel: "{label} {y}",
		dataPoints: [
			{y: 79.45, label: "Google"},
			{y: 7.31, label: "Bing"},
			{y: 7.06, label: "Baidu"},
			{y: 4.91, label: "Yahoo"},
			{y: 1.26, label: "Others"}
		]
	}]
});
chart.render();

}
</script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
-->