<section id="main-content">
  <section class="wrapper">
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>View All Inquires</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div>
    </div>

    <?php
        $status = $_REQUEST['status'];
        echo "<h3>Inquiry Status - ".$status."</h3>";
    ?>

    <div class="row">
        <div class="col-lg-10"></div>
        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_add?id=" target="_blank">
                <input type="button" name="add_new_inq" value="Add New Inquiry" class="form-control">
            </a>
        </div>
    </div><br><br>
    
    <div class="row">
    	<div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>S. No.</th>
                        <th>Inquiry No.</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Inquiry Date</th>
                        <th>Last Updated Date</th>
                        <th>Age</th>
                        <th>Last Conv</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $sql = "SELECT *,datediff(CURDATE(),fran_inq_modified_date) as age FROM franchisee_inq_mst where fran_status = '".$status."'";
                        $qry = $this->db->query($sql);
                        $sno=0;
                        foreach($qry->result() as $row){
                            $sno++;
                    ?>
                    <tr>
                        <td><?=$sno; ?></td>
                        <td>
                            <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_add?id=<?=$row->fran_inq_no; ?>" target="_blank">
                                <?=$row->fran_inq_no; ?>
                            </a>
                        </td>
                        <td><?=$row->fran_inq_name; ?></td>
                        <td><?=$row->fran_inq_phone; ?></td>
                        <td><?=substr($row->fran_date,0,11); ?></td>
                        <td><?=substr($row->fran_inq_modified_date,0,11); ?></td>
                        <td><?=$row->age; ?></td>
                        <td><?=$row->fran_remarks; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
  </section>
</section>