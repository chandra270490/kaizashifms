<style>
	button.accordion {
		background-color:#ddd;
		color: #444;
		cursor: pointer;
		padding: 5px;
		width: 100%;
		border: none;
		text-align: center;
		font-weight:bold;
		outline: none;
		font-size: 14px;
		transition: 0.4s;
		border-radius:8px;
	}
	
	button.accordion.active, button.accordion:hover {
		background-color: #999999;
	}
	
	button.accordion:after {
		content: '\02795';
		font-size: 13px;
		color: #777;
		float: right;
		margin-left: 5px;
	}
	
	button.accordion.active:after {
		content: "\2796";
	}
	
	div.panel {
		padding: 0 5px;
		background-color: white;
		max-height: 0;
		overflow: hidden;
		transition: 0.6s ease-in-out;
		opacity: 0;
		margin-bottom:4px;
	}
	
	div.panel.show {
		opacity: 1;
		max-height: 300px;
	}
	
	table thead tr{
		display:block;
	}
	
	table th,table td{
		width:300px;
	}
	
	table  tbody{		
		display:block;
		height:200px;
		overflow:auto;
	}
</style>

<?php $this->load->helper("franchisees_inq"); ?>

<section id="main-content">
  <section class="wrapper">
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Franchisees Dashboard</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4"></div>

        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_import_inq">
                <input type="button" name="add_new_inq" value="Import Inquires" class="form-control">
            </a>
        </div>

        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_imp_for">
                <input type="button" name="add_new_inq" value="Export Upload Format" class="form-control">
            </a>
        </div>

        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_export">
                <input type="button" name="add_new_inq" value="Export Inquires To XLS" class="form-control">
            </a>
        </div>

        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_add?id=">
                <input type="button" name="add_new_inq" value="Add New Inquiry" class="form-control">
            </a>
        </div>
    </div><br><br>
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-2"></div>
    	<div class="col-lg-8">
            <!-- Accordian Starts -->

            <button class="accordion" style="color:black; font-weight:bold;">
            	1. Fresh inquiry (<?php echo case_count('Fresh Inquiry'); ?>)
            </button>
            <div class="panel">
                <div>
                    <table class="table table-bordered">
                        <tr style="background-color:#ddd; font-weight:bold">
                            <td>Inquiry No.</td>
                            <td>Name</td>
                            <td>Phone</td>
                            <td>Model</td>
                            <td>Date</td>
                        </tr>
                        <?php
                            $sql = "select * from franchisee_inq_mst where fran_status = 'Fresh Inquiry'";
                            $qry = $this->db->query($sql);
                            foreach($qry->result() as $row){
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_add?id=<?=$row->fran_inq_no; ?>">
                                    <?=$row->fran_inq_no; ?>
                                </a>
                            </td>
                            <td><?=$row->fran_inq_name; ?></td>
                            <td><?=$row->fran_inq_phone; ?></td>
                            <td><?=$row->fran_status; ?></td>
                            <td><?=substr($row->fran_date,0,11); ?></td>
                        </tr>
                        <?php        
                            }
                        ?>
                    </table>
                    <a style="text-align:center" href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Fresh Inquiry">
                        <h4>View All</h4>
                    </a>
                </div>
            </div>

            <button class="accordion" style="color:black; font-weight:bold;">
            	2. Under Follow-Up (<?php echo case_count('Under Follow-Up'); ?>)
            </button>
            <div class="panel">
                <div>
                    <table class="table table-bordered">
                        <tr style="background-color:#ddd; font-weight:bold">
                            <td>Inquiry No.</td>
                            <td>Name</td>
                            <td>Phone</td>
                            <td>Model</td>
                            <td>Date</td>
                        </tr>
                        <?php
                            $sql = "select * from franchisee_inq_mst where fran_status = 'Under Follow-Up'";
                            $qry = $this->db->query($sql);
                            foreach($qry->result() as $row){
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_add?id=<?=$row->fran_inq_no; ?>">
                                    <?=$row->fran_inq_no; ?>
                                </a>
                            </td>
                            <td><?=$row->fran_inq_name; ?></td>
                            <td><?=$row->fran_inq_phone; ?></td>
                            <td><?=$row->fran_status; ?></td>
                            <td><?=substr($row->fran_date,0,11); ?></td>
                        </tr>
                        <?php        
                            }
                        ?>
                    </table>
                    <a style="text-align:center" href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Under Follow-Up">
                        <h4>View All</h4>
                    </a>
                </div>
            </div>

            <button class="accordion" style="color:black; font-weight:bold;">
            	3. Under Franchisee Process (<?php echo case_count('Under Franchisee Process'); ?>)
            </button>
            <div class="panel">
                <div>
                    <table class="table table-bordered">
                        <tr style="background-color:#ddd; font-weight:bold">
                            <td>Inquiry No.</td>
                            <td>Name</td>
                            <td>Phone</td>
                            <td>Model</td>
                            <td>Date</td>
                        </tr>
                        <?php
                            $sql = "select * from franchisee_inq_mst where fran_status = 'Under Franchisee Process'";
                            $qry = $this->db->query($sql);
                            foreach($qry->result() as $row){
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_add?id=<?=$row->fran_inq_no; ?>">
                                    <?=$row->fran_inq_no; ?>
                                </a>
                            </td>
                            <td><?=$row->fran_inq_name; ?></td>
                            <td><?=$row->fran_inq_phone; ?></td>
                            <td><?=$row->fran_status; ?></td>
                            <td><?=substr($row->fran_date,0,11); ?></td>
                        </tr>
                        <?php        
                            }
                        ?>
                    </table>
                    <a style="text-align:center" href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Under Franchisee Process">
                        <h4>View All</h4>
                    </a>
                </div>
            </div>

            <button class="accordion" style="color:black; font-weight:bold;">
            	4. Converted (<?php echo case_count('Converted'); ?>)
            </button>
            <div class="panel">
                <div>
                    <table class="table table-bordered">
                        <tr style="background-color:#ddd; font-weight:bold">
                            <td>Inquiry No.</td>
                            <td>Name</td>
                            <td>Phone</td>
                            <td>Model</td>
                            <td>Date</td>
                        </tr>
                        <?php
                            $sql = "select * from franchisee_inq_mst where fran_status = 'Converted'";
                            $qry = $this->db->query($sql);
                            foreach($qry->result() as $row){
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_add?id=<?=$row->fran_inq_no; ?>">
                                    <?=$row->fran_inq_no; ?>
                                </a>
                            </td>
                            <td><?=$row->fran_inq_name; ?></td>
                            <td><?=$row->fran_inq_phone; ?></td>
                            <td><?=$row->fran_status; ?></td>
                            <td><?=substr($row->fran_date,0,11); ?></td>
                        </tr>
                        <?php        
                            }
                        ?>
                    </table>
                    <a style="text-align:center" href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Converted">
                        <h4>View All</h4>
                    </a>
                </div>
            </div>

            <button class="accordion" style="color:black; font-weight:bold;">
            	5. Lost (<?php echo case_count('Lost'); ?>)
            </button>
            <div class="panel">
                <div>
                    <table class="table table-bordered">
                        <tr style="background-color:#ddd; font-weight:bold">
                            <td>Inquiry No.</td>
                            <td>Name</td>
                            <td>Phone</td>
                            <td>Model</td>
                            <td>Date</td>
                        </tr>
                        <?php
                            $sql = "select * from franchisee_inq_mst where fran_status = 'Lost'";
                            $qry = $this->db->query($sql);
                            foreach($qry->result() as $row){
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_add?id=<?=$row->fran_inq_no; ?>">
                                    <?=$row->fran_inq_no; ?>
                                </a>
                            </td>
                            <td><?=$row->fran_inq_name; ?></td>
                            <td><?=$row->fran_inq_phone; ?></td>
                            <td><?=$row->fran_status; ?></td>
                            <td><?=substr($row->fran_date,0,11); ?></td>
                        </tr>
                        <?php        
                            }
                        ?>
                    </table>
                    <a style="text-align:center" href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Lost">
                        <h4>View All</h4>
                    </a>
                </div>
            </div>

            <button class="accordion" style="color:black; font-weight:bold;">
            	6. Dropped (<?php echo case_count('Dropped'); ?>)
            </button>
            <div class="panel">
                <div>
                    <table class="table table-bordered">
                        <tr style="background-color:#ddd; font-weight:bold">
                            <td>Inquiry No.</td>
                            <td>Name</td>
                            <td>Phone</td>
                            <td>Model</td>
                            <td>Date</td>
                        </tr>
                        <?php
                            $sql = "select * from franchisee_inq_mst where fran_status = 'Dropped'";
                            $qry = $this->db->query($sql);
                            foreach($qry->result() as $row){
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_add?id=<?=$row->fran_inq_no; ?>">
                                    <?=$row->fran_inq_no; ?>
                                </a>
                            </td>
                            <td><?=$row->fran_inq_name; ?></td>
                            <td><?=$row->fran_inq_phone; ?></td>
                            <td><?=$row->fran_status; ?></td>
                            <td><?=substr($row->fran_date,0,11); ?></td>
                        </tr>
                        <?php        
                            }
                        ?>
                    </table>
                    <a style="text-align:center" href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Dropped">
                        <h4>View All</h4>
                    </a>
                </div>
            </div>

            <button class="accordion" style="color:black; font-weight:bold;">
            	7. Hold (<?php echo case_count('Hold'); ?>)
            </button>
            <div class="panel">
                <div>
                    <table class="table table-bordered">
                        <tr style="background-color:#ddd; font-weight:bold">
                            <td>Inquiry No.</td>
                            <td>Name</td>
                            <td>Phone</td>
                            <td>Model</td>
                            <td>Date</td>
                        </tr>
                        <?php
                            $sql = "select * from franchisee_inq_mst where fran_status = 'Hold'";
                            $qry = $this->db->query($sql);
                            foreach($qry->result() as $row){
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_add?id=<?=$row->fran_inq_no; ?>">
                                    <?=$row->fran_inq_no; ?>
                                </a>
                            </td>
                            <td><?=$row->fran_inq_name; ?></td>
                            <td><?=$row->fran_inq_phone; ?></td>
                            <td><?=$row->fran_status; ?></td>
                            <td><?=substr($row->fran_date,0,11); ?></td>
                        </tr>
                        <?php        
                            }
                        ?>
                    </table>
                    <a style="text-align:center" href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Hold">
                        <h4>View All</h4>
                    </a>
                </div>
            </div>
            
            <!-- Accordian Ends -->
        </div>
    	<div class="col-lg-2"></div>
    </div>
  </section>
</section>

<!-- According Javascript -->
<script type="text/javascript">
	var acc = document.getElementsByClassName("accordion");
	var i;
	for (i = 0; i < acc.length; i++) {
		acc[i].onclick = function(){
			this.classList.toggle("active");
			this.nextElementSibling.classList.toggle("show");
	  }
	}
</script>