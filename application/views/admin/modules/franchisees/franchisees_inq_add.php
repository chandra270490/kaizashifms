<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Add Franchise Inquiry</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <?php
        $fran_inq_no = $_REQUEST['id'];

        if($fran_inq_no != ""){

            $sql_fran_inq = "select * from franchisee_inq_mst where fran_inq_no = '".$fran_inq_no."'";
            $qry_fran_inq = $this->db->query($sql_fran_inq)->row();

            $fran_date         = $qry_fran_inq->fran_date;
            $fran_inq_name     = $qry_fran_inq->fran_inq_name;
            $fran_inq_phone    = $qry_fran_inq->fran_inq_phone;
            $fran_inq_whatsapp = $qry_fran_inq->fran_inq_whatsapp;
            $fran_inq_email    = $qry_fran_inq->fran_inq_email;
            $fran_inq_addr1    = $qry_fran_inq->fran_inq_addr1;
            $fran_inq_city     = $qry_fran_inq->fran_inq_city;
            $fran_inq_state    = $qry_fran_inq->fran_inq_state;
            $fran_inq_pin      = $qry_fran_inq->fran_inq_pin;
            $fran_inq_country  = $qry_fran_inq->fran_inq_country;
            $fran_wfm_pref_loc = $qry_fran_inq->fran_wfm_pref_loc;
            $fran_model        = $qry_fran_inq->fran_model;
            $fran_lead_source  = $qry_fran_inq->fran_lead_source;
            $fran_status       = $qry_fran_inq->fran_status;
            $fran_remarks      = $qry_fran_inq->fran_remarks;

        } else {

            $fran_date         = "";
            $fran_inq_name     = "";
            $fran_inq_phone    = "";
            $fran_inq_whatsapp = "";
            $fran_inq_email    = "";
            $fran_inq_addr1    = "";
            $fran_inq_city     = "";
            $fran_inq_state    = "";
            $fran_inq_pin      = "";
            $fran_inq_country  = "";
            $fran_wfm_pref_loc = "";
            $fran_model        = "";
            $fran_lead_source  = "";
            $fran_status       = "";
            $fran_remarks      = "";

        }
    ?>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Add Franchisee Inquiry</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_entry">
                        <?php
                            if($fran_inq_no != ""){
                                echo "<h3 style='text-align:center'>Inquiry No - ".$fran_inq_no."</h3><br>";
                                echo "<input type='hidden' name='fran_inq_no' id='fran_inq_no' value='".$fran_inq_no."'>";
                            } else {
                                echo "<input type='hidden' name='fran_inq_no' id='fran_inq_no' value=''>";
                            }
                        ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fran_date" name="fran_date" 
                                value="<?=$fran_date; ?>" required readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fran_inq_name" name="fran_inq_name" 
                                value="<?=$fran_inq_name; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fran_inq_phone" name="fran_inq_phone" 
                                value="<?=$fran_inq_phone; ?>" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Whatsapp No.</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fran_inq_whatsapp" name="fran_inq_whatsapp" 
                                value="<?=$fran_inq_whatsapp; ?>" onkeypress="return isNumberKey(event);">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fran_inq_email" name="fran_inq_email" 
                                value="<?=$fran_inq_email; ?>" required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fran_inq_addr1" name="fran_inq_addr1" 
                                value="<?=$fran_inq_addr1; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">City / District</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fran_inq_city" name="fran_inq_city" 
                                value="<?=$fran_inq_city; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">State</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="fran_inq_state" name="fran_inq_state" required>
                                    <?php
                                        if($fran_inq_state != ""){
                                    ?>
                                    <option value="<?=$fran_inq_state;?>"><?=$fran_inq_state;?></option>
                                    <?php        
                                        }
                                    ?>
                                    <option value="">--Select--</option>
                                    <?php
                                        $sql_state = "select * from state_mst";
                                        $qry_state = $this->db->query($sql_state);
                                        
                                        foreach($qry_state->result() as $row){
                                            $state_name = $row->state_name;
                                    ?>
                                    <option value="<?=$state_name; ?>"><?=$state_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Postal Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fran_inq_pin" name="fran_inq_pin" 
                                value="<?=$fran_inq_pin; ?>" onkeypress="return isNumberKey(event);" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Country</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="fran_inq_country" name="fran_inq_country" required>
                                    <?php
                                        if($fran_inq_country != ""){
                                    ?>
                                    <option value="<?=$fran_inq_country;?>"><?=$fran_inq_country;?></option>
                                    <?php        
                                        }
                                    ?>
                                    <option value="">--Select--</option>
                                    <?php
                                        $sql_country = "select * from country_mst";
                                        $qry_country = $this->db->query($sql_country);
                                        
                                        foreach($qry_country->result() as $row){
                                            $country_name = $row->country_name;
                                    ?>
                                    <option value="<?=$country_name; ?>"><?=$country_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Preffered Location Of WFM Store</label>
                            <div class="col-sm-10">
                                <input type="text" id="fran_wfm_pref_loc" name="fran_wfm_pref_loc" class="form-control" 
                                value="<?=$fran_wfm_pref_loc; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Preffered Franchise Model</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="fran_model" name="fran_model" required>
                                    <?php
                                        if($fran_model != ""){
                                    ?>
                                    <option value="<?=$fran_model;?>"><?=$fran_model;?></option>
                                    <?php        
                                        }
                                    ?>
                                    <option value="">--Select--</option>
                                    <?php
                                        $sql_model = "select * from franchisee_model_master";
                                        $qry_model = $this->db->query($sql_model);
                                        
                                        foreach($qry_model->result() as $row){
                                            $id = $row->id;
                                            $model_name = $row->model_name;
                                    ?>
                                    <option value="<?=$model_name; ?>"><?=$model_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Lead Source</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="fran_lead_source" name="fran_lead_source" required>
                                    <?php
                                        if($fran_lead_source != ""){
                                    ?>
                                    <option value="<?=$fran_lead_source;?>"><?=$fran_lead_source;?></option>
                                    <?php        
                                        }
                                    ?>
                                    <option value="">--Select--</option>
                                    <option value="Direct">Direct</option>
                                    <option value="Website">Website</option>
                                    <option value="Social Media">Social Media</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="fran_status" name="fran_status" required>
                                    <?php
                                        if($fran_status != ""){
                                    ?>
                                    <option value="<?=$fran_status;?>"><?=$fran_status;?></option>
                                    <?php        
                                        }
                                    ?>
                                    <option value="">--Select--</option>
                                    <option value="Fresh Inquiry">Fresh Inquiry</option>
                                    <option value="Under Follow-up">Under Follow-up</option>
                                    <option value="Converted">Converted</option>
                                    <option value="Dropped">Dropped</option>
                                    <option value="Lost">Lost</option>
                                    <option value="Under Franchisee Process">Under Franchisee Process</option>
                                    <option value="Hold">Hold</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Remarks</label>
                            <div class="col-sm-10">
                                <textarea id="fran_remarks" name="fran_remarks" class="form-control"><?=$fran_remarks; ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                        
                        <?php if($fran_inq_no != "") { ?>
                        <div class="form-group">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-10">
                                <h3>Conversations</h3>
                                <table class="table table-bordered" style="text-align:left">
                                    <thead>
                                        <tr>
                                            <th>SNO.</th>
                                            <th>Inquiry No.</th>
                                            <th>Converstion</th>
                                            <th>Conversation By</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $sql_conv = "select * from franchisee_rmk_hist where fran_inq_no = '".$fran_inq_no."'";
                                            $qry_conv = $this->db->query($sql_conv);

                                            $sno=0;
                                            foreach($qry_conv->result() as $row){
                                                $sno++;
                                                $fran_inq_no = $row->fran_inq_no;
                                                $fran_remarks = $row->fran_remarks;
                                                $created_by = $row->created_by;
                                                $datetime = $row->datetime;
                                        ?>
                                        <tr>
                                            <td><?=$sno;?></td>
                                            <td><?=$fran_inq_no;?></td>
                                            <td><?=$fran_remarks;?></td>
                                            <td><?=$created_by;?></td>
                                            <td><?=$datetime;?></td>
                                        </tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-1"></div>
                        </div>
                        <?php } ?>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>
//select 2 box
$( function(){
    $("#fran_inq_country").select2();	
    $("#fran_inq_state").select2();
});

//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}

//Datepicker
$( function() {
    $( "#fran_date" ).datepicker({
        "dateFormat" : "yy-mm-dd"
    });
} );
</script>