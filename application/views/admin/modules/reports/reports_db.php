<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Reports Dashboard</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/reportsc/customers">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Customers
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/reportsc/franchisee_inquiry">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Franchisee Inquiry
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/reportsc/franchisee_inprocess">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Franchisee In-Process
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/reportsc/franchisee_inline">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Franchisee Inline
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/reportsc/master_franchisee">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Master Franchisee
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/reportsc/online_orders">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Online Orders
            </a>
        </div>
    </div><br><br>

    <div class="row" style="text-align:center">
    	<div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/reportsc/pending_po">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Pending PO
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/reportsc/products">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Products
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/reportsc/sales">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Sales
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/reportsc/stat_updt">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Statistics Update
            </a>
        </div>
    </div>

  </section>
</section>