<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Sales List</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <!-- Add Row Button -->    
    <!-- View Records -->
    <div class="row">
        <div class="col-lg-12">
        	<table class="table table-bordered">
                <!-- ListHead Starts -->
                <thead>
                    <tr>
                        <th>SNo</th>
                        <th>Sales Store Id</th>
                        <th>Sales Order No</th>
                        <th>Sales Order Date</th>
                        <th>Sales Invoice Date</th>
                        <th>Sales Invoice Id</th>
                        <th>Sales Bill No</th>
                        <th>Sales Customer</th>
                        <th>Sales Phone</th>
                        <th>Sales Email</th>
                        <th>Sales Barcode</th>
                        <th>Sales Category</th>
                        <th>Sales Product</th>
                    </tr>
                </thead>
                <!-- ListHead Ends -->
                <!-- ListBody Starts -->
                <tbody>
                    <?php
                        $sql_tbl_val = "SELECT * FROM sales_mst where sales_store_id != ''";
                        $qry_tbl_val = $this->db->query($sql_tbl_val);

                        $sno=0;
                        foreach($qry_tbl_val->result() as $row){
                            $sno++;
                    ?>
                    <tr>
                        <td><?php echo $sno; ?></td>
                        <td><?php echo $row->sales_store_id; ?></td>
                        <td><?php echo $row->sales_order_no; ?></td>
                        <td><?php echo $row->sales_order_date; ?></td>
                        <td><?php echo $row->sales_invoice_date; ?></td>
                        <td><?php echo $row->sales_invoice_id; ?></td>
                        <td><?php echo $row->sales_bill_no; ?></td>
                        <td><?php echo $row->sales_customer; ?></td>
                        <td><?php echo $row->sales_phone; ?></td>
                        <td><?php echo $row->sales_email; ?></td>
                        <td><?php echo $row->sales_barcode; ?></td>
                        <td><?php echo $row->sales_category; ?></td>
                        <td><?php echo $row->sales_product; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                <!-- ListBody Ends -->
            </table>
        </div>
    </div>
  </section>
</section>