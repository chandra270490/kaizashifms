<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i> Product Category Add</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Add Product Category</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" 
                    action="<?php echo base_url(); ?>index.php/settingsc/prodcat_entry">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Product Category</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="prodcat_name" name="prodcat_name" 
                                value="" required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>