<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('sai')){	
    
    function raw_mat_list(){
		$ci =& get_instance();
		$ci->load->database();
			
        $sql="select * from raw_mat_mst where product_active = 'Yes'";
		
		$query = $ci->db->query($sql);
        
        $data = '<option value="">--select--</option>';
		foreach ($query->result() as $row) {
		  $product_id = $row->product_id;
          $product_name = $row->product_name;
          
          $data .= '<option value="'.$product_id.'">'.$product_name.'</option>';

		}
		
		return $data;
	}

	function category_list(){
		$ci =& get_instance();
		$ci->load->database();
			
        $sql="select * from category_mst where category_active = 'Yes'";
		
		$query = $ci->db->query($sql);
        
        $data = '<option value="">--select--</option>';
		foreach ($query->result() as $row) {
		  $category_id = $row->category_id;
          $category_name = $row->category_name;
          
          $data .= '<option value="'.$category_id.'">'.$category_name.'</option>';

		}
		
		return $data;
	}

	//Products By Category Id
	function cat_prod_list($category_id){
		$ci =& get_instance();
		$ci->load->database();
			
        $sql="select * from raw_mat_mst where product_active = 'Yes' and category_id = '".$category_id."'";
		
		$query = $ci->db->query($sql);
        
        $data = '<option value="">--select--</option>';
		foreach ($query->result() as $row) {
		  $product_id = $row->product_id;
          $product_name = $row->product_name;
          
          $data .= '<option value="'.$product_id.'">'.$product_name.'</option>';

		}
		
		return $data;
	}	
}