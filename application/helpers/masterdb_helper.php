<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('masterdb'))
{
	//Start of helper functions

	//Dashboard Counter
	function db_value($tbl_nm){
		$ci =& get_instance();
		$ci->load->database();
        
        $count_query1="select count(*) as count1 from $tbl_nm";
		
		$row_count = $ci->db->query($count_query1)->row();
		
        $count1 = $row_count->count1;	
        
		return $count1;	
	}

	//Master Franchise Count
	function db_mst_frac($tbl_nm){
		$ci =& get_instance();
		$ci->load->database();
        
        $count_query1="select count(*) as count1 from $tbl_nm where franchisee_type = 'master'";
		
		$row_count = $ci->db->query($count_query1)->row();
		
        $count1 = $row_count->count1;	
        
		return $count1;	
	}

	//Number Of Stores
	function db_store(){
		$ci =& get_instance();
		$ci->load->database();
        
        $count_query1="SELECT count(distinct sales_store_id) as count1 FROM kaizashifms.sales_mst where sales_store_id != ''";
		
		$row_count = $ci->db->query($count_query1)->row();
		
        $count1 = $row_count->count1;	
        
		return $count1;	
	}

	//Products Total
	function db_products(){
		$ci =& get_instance();
		$ci->load->database();
        
        $count_query1="SELECT count(distinct(sales_product)) as count1 FROM sales_mst";
		
		$row_count = $ci->db->query($count_query1)->row();
		
        $count1 = $row_count->count1;	
        
		return $count1;		
	}

	//Sales Total
	function db_sales(){
		$ci =& get_instance();
		$ci->load->database();
        
        $count_query1="SELECT sum(sales_subtotal) as total_sales FROM kaizashifms.sales_mst where sales_store_id != ''";
		
		$row_count = $ci->db->query($count_query1)->row();
		
        $total_sales = $row_count->total_sales;	
        
		return $total_sales;	
	}
	
	//Out Of Stock Products
	function oos_value($tbl_nm){
		$ci =& get_instance();
		$ci->load->database();
        
        $count_query1="select count(*) as count1 from $tbl_nm where prod_stock = 0";
		
		$row_count = $ci->db->query($count_query1)->row();
		
        $count1 = $row_count->count1;	
        
		return $count1;	
	}
	
	function fran_inq($tbl_nm){

		$ci =& get_instance();
		$ci->load->database();
        
        $count_query1="select count(*) as count1 from $tbl_nm";
		
		$row_count = $ci->db->query($count_query1)->row();
		
        $count1 = $row_count->count1;	
        
		return $count1;	

	}

	function store_sales($store_name){
		$ci =& get_instance();
		$ci->load->database();

		$sql = "select max(sales) as tot_sales from store_stats  
		where store_name = '".$store_name."'
		and created_date = (select max(created_date) from store_stats 
		where store_name in('".$store_name."'))";

		$qry = $ci->db->query($sql)->row();

		$tot_sales = $qry->tot_sales;

		return number_format($tot_sales,2);

	}

	function store_cust($store_name){
		$ci =& get_instance();
		$ci->load->database();

		$sql = "select max(customers) as tot_cust from store_stats 
		where store_name = '".$store_name."'
		and created_date = (select max(created_date) from store_stats 
		where store_name in('".$store_name."'))";

		$qry = $ci->db->query($sql)->row();

		$tot_cust = $qry->tot_cust;

		return $tot_cust;

	}

	function tot_sales(){
		$ci =& get_instance();
		$ci->load->database();

		$sql = "select sum(sales) as tot_sales from store_stats 
		where store_name in('RJ0101','RJ0102', 'RJ1401', 'RJ0103', 'RJ1901') 
		and created_date = (select max(created_date) from store_stats 
		where store_name in('RJ0101','RJ0102', 'RJ1401', 'RJ0103', 'RJ1901'))";

		$qry = $ci->db->query($sql)->row();

		$tot_sales = $qry->tot_sales;

		return number_format($tot_sales,2);

	}

	function tot_cust(){
		$ci =& get_instance();
		$ci->load->database();

		$sql = "select sum(customers) as tot_cust from store_stats 
		where store_name in('RJ0101','RJ0102', 'RJ1401', 'RJ0103', 'RJ1901') 
		and created_date = (select max(created_date) from store_stats 
		where store_name in('RJ0101','RJ0102', 'RJ1401', 'RJ0103', 'RJ1901'))";

		$qry = $ci->db->query($sql)->row();

		$tot_cust = $qry->tot_cust;

		return $tot_cust;

	}

	//End of helper functions		
}