<?php
class Saim extends CI_Model{  
	function __construct(){   
		parent::__construct();  
	}

	//View List
	function ListHead($tbl_nm){
        $query = $this->db->query("SHOW columns FROM $tbl_nm");
        return $query;
    }

    //Vendor Entry
    function vendor_entry($data){
        $vendor_name    = $this->input->post("vendor_name");
        $vendor_address = $this->input->post("vendor_address");
        $vendor_city    = $this->input->post("vendor_city");
        $vendor_state   = $this->input->post("vendor_state");
        $vendor_country   = $this->input->post("vendor_country");
        $cp_owner_name   = $this->input->post("cp_owner_name");
        $cp_owner_phone   = $this->input->post("cp_owner_phone");
        $cp_owner_email  = $this->input->post("cp_owner_email");
        $cp_acc_name  = $this->input->post("cp_acc_name");
        $cp_acc_phone  = $this->input->post("cp_acc_phone");
        $cp_acc_email  = $this->input->post("cp_acc_email");
        $vendor_pan  = $this->input->post("vendor_pan");
        $vendor_gst  = $this->input->post("vendor_gst");
        $vendor_reg_msme  = $this->input->post("vendor_reg_msme");
        $vendor_active  = $this->input->post("vendor_active");

        //Session Details
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by = $_SESSION['username'];

        //Trim
		$vendor_name = trim($vendor_name);
		$vendor_address = trim($vendor_address);
		$vendor_city = trim($vendor_city);
		$vendor_state = trim($vendor_state);
		$vendor_country   = trim($vendor_country);
        $cp_owner_name   = trim($cp_owner_name);
        $cp_owner_phone   = trim($cp_owner_phone);
        $cp_owner_email  = trim($cp_owner_email);
        $cp_acc_name  = trim($cp_acc_name);
        $cp_acc_phone  = trim($cp_acc_phone);
        $cp_acc_email  = trim($cp_acc_email);
        $vendor_pan  = trim($vendor_pan);
        $vendor_gst  = trim($vendor_gst);
        $vendor_reg_msme  = trim($vendor_reg_msme);
        $vendor_active  = trim($vendor_active);

		//Transaction Start
		$this->db->trans_start();

        $sql = $this->db->query("insert into vendor_mst
        (vendor_name, vendor_address, vendor_city, 
        vendor_state, vendor_country, cp_owner_name, 
        cp_owner_phone, cp_owner_email, cp_acc_name, 
        cp_acc_phone, cp_acc_email, vendor_pan, 
        vendor_gst, vendor_reg_msme, vendor_active,
        created_by, created_date, modified_by) 
		values 
        ('".$vendor_name."', '".$vendor_address."', '".$vendor_city."', 
        '".$vendor_state."', '".$vendor_country."', '".$cp_owner_name."',
        '".$cp_owner_phone."', '".$cp_owner_email."', '".$cp_acc_name."',
        '".$cp_acc_phone."', '".$cp_acc_email."', '".$vendor_pan."',
        '".$vendor_gst."', '".$vendor_reg_msme."', '".$vendor_active."',
        '".$created_by."', '".$created_date."', '".$modified_by."')");

		$this->db->trans_complete();
    }

    //GST Entry
    function gst_entry($data){
        $gst_name    = $this->input->post("gst_name");
        $gst_perc    = $this->input->post("gst_perc");
        $gst_active  = $this->input->post("gst_active");

        //Session Details
		$created_by   = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by  = $_SESSION['username'];

        //Trim
		$gst_name   = trim($gst_name);
		$gst_perc   = trim($gst_perc);
		$gst_active = trim($gst_active);

		//Transaction Start
		$this->db->trans_start();

        $sql = $this->db->query("insert into gst_mst(gst_name, gst_perc, gst_active, 
        created_by, created_date, modified_by) 
		values 
        ('".$gst_name."', '".$gst_perc."', '".$gst_active."', 
        '".$created_by."', '".$created_date."', '".$modified_by."')");

		$this->db->trans_complete();
    }

    //Category Entry
    function category_entry($data){
        $category_name    = $this->input->post("category_name");
        $category_active  = $this->input->post("category_active");

        //Session Details
		$created_by   = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by  = $_SESSION['username'];

        //Trim
		$category_name   = trim($category_name);
		$category_active = trim($category_active);

		//Transaction Start
		$this->db->trans_start();

        $sql = $this->db->query("insert into category_mst(category_name, category_active, 
        created_by, created_date, modified_by) 
		values 
        ('".$category_name."', '".$category_active."', 
        '".$created_by."', '".$created_date."', '".$modified_by."')");

		$this->db->trans_complete();
    }

    //Raw Material Entry
    function raw_mat_entry($data){
        $product_name    = $this->input->post("product_name");
        $category_id    = $this->input->post("category_id");
        $product_active  = $this->input->post("product_active");

        //Session Details
		$created_by   = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by  = $_SESSION['username'];

        //Trim
		$product_name   = trim($product_name);
		$product_active = trim($product_active);

		//Transaction Start
		$this->db->trans_start();

        $sql = $this->db->query("insert into raw_mat_mst(product_name, category_id, product_active, 
        created_by, created_date, modified_by) 
		values 
        ('".$product_name."', '".$category_id."', '".$product_active."', 
        '".$created_by."', '".$created_date."', '".$modified_by."')");

		$this->db->trans_complete();
    }

    //Bill Entry
    function bill_entry($data){
        $bill_mst_id    = $this->input->post("bill_mst_id");
        $vendor_id  = $this->input->post("vendor_id");
        $invoice_no  = $this->input->post("invoice_no");
        $invoice_date  = $this->input->post("invoice_date");
        $tot_qty  = $this->input->post("tot_qty");
        $tot_amt  = $this->input->post("tot_amt");

        $category_id  = $this->input->post("category_id");
        $product_id  = $this->input->post("product_id");
        $quantity  = $this->input->post("quantity");
        $rate  = $this->input->post("rate");
        $line_total_amt  = $this->input->post("line_total_amt");
        
        $count = count($product_id);
        //Session Details
		$created_by   = $_SESSION['username'];
		$created_date = date("Y-m-d h:i:s");
		$modified_by  = $_SESSION['username'];

        //Trim
		$invoice_no   = trim($invoice_no);

		//Transaction Start
		$this->db->trans_start();

        $sql = $this->db->query("insert into bill_mst(vendor_id, invoice_no, invoice_date, tot_qty, tot_amt,
        created_by, created_date, modified_by) 
		values 
        ('".$vendor_id."', '".$invoice_no."', '".$invoice_date."', '".$tot_qty."', '".$tot_amt."', 
        '".$created_by."', '".$created_date."', '".$modified_by."')");

        //Updating Vendor Name
        $sql_ven_nm = $this->db->query("update bill_mst 
        inner join vendor_mst on vendor_mst.vendor_id = bill_mst.vendor_id
        set bill_mst.vendor_name = vendor_mst.vendor_name 
        where bill_mst.vendor_id = '".$vendor_id."'");

        //Get Max bill id
        $sql_max_id = "select max(bill_mst_id) as max_id from bill_mst";
        $qry_max_id = $this->db->query($sql_max_id)->row();
        $max_id = $qry_max_id->max_id;

        //inserting items
        for($i=0;$i<$count;$i++){

            $sql_itm_ins = $this->db->query("insert into bill_dtl (bill_mst_id, category_id, product_id, quantity, 
            rate, total_amt, created_by, 
            created_date, modified_by)
            values('".$max_id."', '".$category_id[$i]."', '".$product_id[$i]."', '".$quantity[$i]."', 
            '".$rate[$i]."', '".$line_total_amt[$i]."', '".$created_by."',
            '".$created_date."', '".$modified_by."')");

        }

		$this->db->trans_complete();
    }
	 
}  
?>