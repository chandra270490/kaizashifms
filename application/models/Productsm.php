<?php
class Productsm extends CI_Model{  
	function __construct(){   
		parent::__construct();  
	}
	 
	function ListHead($tbl_nm){
        $query = $this->db->query("SHOW columns FROM $tbl_nm");

        return $query;
    }
	  
	/*************************************/
	/***Products***/
	/*************************************/

	//Product Entry
	public function products_entry($data){ 
		$prod_comp_id = $this->input->post("prod_comp_id");
		$prod_cat_id = $this->input->post("prod_cat_id");
		$prod_name = $this->input->post("prod_name");
		$prod_code = $this->input->post("prod_code");
		$prod_desc = $this->input->post("prod_desc");
		$prod_unit_price = $this->input->post("prod_unit_price");
		$prod_stock = $this->input->post("prod_stock");
		$prod_enabled = 1;
		$prod_createdby  = $_SESSION['username'];
		$prod_createddate = date('Y-m-d H:i:s');
		$prod_modifiedby = $_SESSION['username'];
		$prod_modifieddate = date('Y-m-d H:i:s');

		//trim values
		$prod_comp_id = trim($prod_comp_id);
		$prod_cat_id = trim($prod_cat_id);
		$prod_name = trim($prod_name);
		$prod_code = trim($prod_code);
		$prod_desc = trim($prod_desc);
		$prod_unit_price = trim($prod_unit_price);
		$prod_stock = trim($prod_stock);

		//Transaction Start
		$this->db->trans_start();

		$sql = "insert into products_mst(prod_comp_id, prod_cat_id, prod_name, prod_code,
		prod_desc, prod_unit_price, prod_stock, prod_enabled,
		prod_createdby, prod_createddate, prod_modifiedby, prod_modifieddate) 
		values 
		('".$prod_comp_id."', '".$prod_cat_id."', '".$prod_name."', '".$prod_code."', 
		'".$prod_desc."', '".$prod_unit_price."', '".$prod_stock."', '".$prod_enabled."',  
		'".$prod_createdby."', '".$prod_createddate."', '".$prod_modifiedby."', '".$prod_modifieddate."')";

		$this->db->query($sql);

		$this->db->trans_complete();
		//Transanction Complete
	 }

	//Excel Import
	public function saverecords($fname,$lname)
	{
		$query="insert into user1 values('$fname','$lname')";
		$this->db->query($query);
	}
	 
   }  
?>