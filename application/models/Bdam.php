<?php
class Bdam extends CI_Model{  
	function __construct(){   
		parent::__construct();  
	}

	//View List
	function ListHead($tbl_nm){
        $query = $this->db->query("SHOW columns FROM $tbl_nm");

        return $query;
    }
	  
	/*************************************/
	/***Business Associate***/
	/*************************************/

	//Businees Associate Entry
	public function bda_entry($data){ 
		$bda_name = $this->input->post("bda_name");
		$bda_phone = $this->input->post("bda_phone");
		$bda_email = $this->input->post("bda_email");
		$bda_compname = $this->input->post("bda_compname");
		$bda_addr1 = $this->input->post("bda_addr1");
		$bda_addr2 = $this->input->post("bda_addr2");
		$bda_city = $this->input->post("bda_city");
		$bda_state = $this->input->post("bda_state");
		$bda_postcode = $this->input->post("bda_postcode");
		$bda_country = $this->input->post("bda_country");
		$bda_adhar = $this->input->post("bda_adhar");
		$bda_pan = $this->input->post("bda_pan");
		$bda_bankacname = $this->input->post("bda_bankacname");
		$bda_bankname = $this->input->post("bda_bankname");
		$bda_accountno = $this->input->post("bda_accountno");
		$bda_ifsc = $this->input->post("bda_ifsc");
		$bda_created_by  = $_SESSION['username'];
		$bda_created_date = date('Y-m-d H:i:s');
		$bda_modified_by = $_SESSION['username'];
		$bda_modified_date = date('Y-m-d H:i:s');

		//Transaction Start
		$this->db->trans_start();

		$sql = "insert into bda_mst(bda_name, bda_phone, bda_email, bda_compname,
		bda_addr1, bda_addr2, bda_city, bda_state,
		bda_postcode, bda_country, bda_adhar, bda_pan,
		bda_bankacname, bda_bankname, bda_accountno, bda_ifsc,
		bda_created_by, bda_created_date, bda_modified_by, bda_modified_date) 
		values 
		('".$bda_name."', '".$bda_phone."', '".$bda_email."', '".$bda_compname."', 
		'".$bda_addr1."', '".$bda_addr2."', '".$bda_city."', '".$bda_state."', 
		'".$bda_postcode."', '".$bda_country."', '".$bda_adhar."', '".$bda_pan."', 
		'".$bda_bankacname."', '".$bda_bankname."', '".$bda_accountno."', '".$bda_ifsc."', 
		'".$bda_created_by."', '".$bda_created_date."', '".$bda_modified_by."', '".$bda_modified_date."')";

		$this->db->query($sql);

		$this->db->trans_complete();
		//Transanction Complete
	 }

	 /*************************************/
	/***Commission Agent***/
	/*************************************/

	//Commission Agent Entry
	public function comagnt_entry($data){ 
		$comagnt_name = $this->input->post("comagnt_name");
		$comagnt_phone = $this->input->post("comagnt_phone");
		$comagnt_email = $this->input->post("comagnt_email");
		$comagnt_compname = $this->input->post("comagnt_compname");
		$comagnt_addr1 = $this->input->post("comagnt_addr1");
		$comagnt_addr2 = $this->input->post("comagnt_addr2");
		$comagnt_city = $this->input->post("comagnt_city");
		$comagnt_state = $this->input->post("comagnt_state");
		$comagnt_postcode = $this->input->post("comagnt_postcode");
		$comagnt_country = $this->input->post("comagnt_country");
		$comagnt_adhar = $this->input->post("comagnt_adhar");
		$comagnt_pan = $this->input->post("comagnt_pan");
		$comagnt_bankacname = $this->input->post("comagnt_bankacname");
		$comagnt_bankname = $this->input->post("comagnt_bankname");
		$comagnt_accountno = $this->input->post("comagnt_accountno");
		$comagnt_ifsc = $this->input->post("comagnt_ifsc");
		$comagnt_created_by  = $_SESSION['username'];
		$comagnt_created_date = date('Y-m-d H:i:s');
		$comagnt_modified_by = $_SESSION['username'];
		$comagnt_modified_date = date('Y-m-d H:i:s');

		//Trim Values
		$comagnt_name = trim($comagnt_name);
		$comagnt_phone = trim($comagnt_phone);
		$comagnt_email = trim($comagnt_email);
		$comagnt_compname = trim($comagnt_compname);
		$comagnt_addr1 = trim($comagnt_addr1);
		$comagnt_addr2 = trim($comagnt_addr2);
		$comagnt_city = trim($comagnt_city);
		$comagnt_state = trim($comagnt_state);
		$comagnt_postcode = trim($comagnt_postcode);
		$comagnt_country = trim($comagnt_country);
		$comagnt_adhar = trim($comagnt_adhar);
		$comagnt_pan = trim($comagnt_pan);
		$comagnt_bankacname = trim($comagnt_bankacname);
		$comagnt_bankname = trim($comagnt_bankname);
		$comagnt_accountno = trim($comagnt_accountno);
		$comagnt_ifsc = trim($comagnt_ifsc);

		//Transaction Start
		$this->db->trans_start();

		$sql = "insert into comagnt_mst(comagnt_name, comagnt_phone, comagnt_email, comagnt_compname,
		comagnt_addr1, comagnt_addr2, comagnt_city, comagnt_state,
		comagnt_postcode, comagnt_country, comagnt_adhar, comagnt_pan,
		comagnt_bankacname, comagnt_bankname, comagnt_accountno, comagnt_ifsc,
		comagnt_created_by, comagnt_created_date, comagnt_modified_by, comagnt_modified_date) 
		values 
		('".$comagnt_name."', '".$comagnt_phone."', '".$comagnt_email."', '".$comagnt_compname."', 
		'".$comagnt_addr1."', '".$comagnt_addr2."', '".$comagnt_city."', '".$comagnt_state."', 
		'".$comagnt_postcode."', '".$comagnt_country."', '".$comagnt_adhar."', '".$comagnt_pan."', 
		'".$comagnt_bankacname."', '".$comagnt_bankname."', '".$comagnt_accountno."', '".$comagnt_ifsc."', 
		'".$comagnt_created_by."', '".$comagnt_created_date."', '".$comagnt_modified_by."', '".$comagnt_modified_date."')";

		$this->db->query($sql);

		$this->db->trans_complete();
		//Transanction Complete
	 }
	  
	 
   }  
?>