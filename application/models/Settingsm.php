<?php
class Settingsm extends CI_Model{  
      function __construct(){   
         parent::__construct();  
	  }

		//List Columns
		function ListHead($tbl_nm){
			$query = $this->db->query("SHOW columns FROM $tbl_nm");
			return $query;
		}
	  
	/*************************************/
	/***Company Module***/
	/*************************************/
		
	  //Company Entry
	  public function company_entry($data, $NewFileName2){ 
		$sql_comp_cnt = "select count(*) as count from company_mst";
		$qry_comp_cnt = $this->db->query($sql_comp_cnt)->row();
		$count = $qry_comp_cnt->count;

		if($count == 0){
			//SVIPL-COM-2020-00001;
			$comp_no = "SVIPL-COM-".date("Y")."-".sprintf('%05d', 1);
		} else {
			$sql_comp_max = "select max(substring(comp_no,16,5)) as prev_no from company_mst";
			$qry_comp_max = $this->db->query($sql_comp_max)->row();
			$prev_no = $qry_comp_max->prev_no;
			$new_no = $prev_no+1;
			
			$comp_no = "SVIPL-COM-".date("Y")."-".sprintf('%05d', $new_no);
		}

		$comp_fra_name = $this->input->post("comp_fra_name");
		$comp_email = $this->input->post("comp_email");
		$comp_name = $this->input->post("comp_name");
		$comp_logo = $this->input->post("comp_logo");
		$comp_logo = $NewFileName2;
		$comp_website = $this->input->post("comp_website");
		$comp_phone = $this->input->post("comp_phone");
		$comp_addr1 = $this->input->post("comp_addr1");
		$comp_addr2 = $this->input->post("comp_addr2");
		$comp_city  = $this->input->post("comp_city");
		$comp_state = $this->input->post("comp_state");
		$comp_postcode = $this->input->post("comp_postcode");
		$comp_country = $this->input->post("comp_country");
		$comp_created_by  = $_SESSION['username'];
		$comp_created_date = date('Y-m-d H:i:s');
		$comp_modified_by  = $_SESSION['username'];
		$comp_modified_date = date('Y-m-d H:i:s');

		//trim
		$comp_fra_name = trim($comp_fra_name);
		$comp_email = trim($comp_email);
		$comp_name = trim($comp_name);
		$comp_logo = trim($comp_logo);
		$comp_logo = $NewFileName2;
		$comp_website = trim($comp_website);
		$comp_phone = trim($comp_phone);
		$comp_addr1 = trim($comp_addr1);
		$comp_addr2 = trim($comp_addr2);
		$comp_city  = trim($comp_city);
		$comp_state = trim($comp_state);
		$comp_postcode = trim($comp_postcode);
		$comp_country = trim($comp_country);

		//Transaction Start
		$this->db->trans_start();

		$sql = "insert into company_mst(comp_no, comp_fra_name, comp_email, comp_name, comp_logo, 
		comp_website, comp_phone, comp_addr1, comp_addr2, 
		comp_city, comp_state, comp_postcode, comp_country, 
		comp_created_by,comp_created_date,comp_modified_by,comp_modified_date) 
		values 
		('".$comp_no."', '".$comp_fra_name."', '".$comp_email."', '".$comp_name."', '".$comp_logo."', 
		'".$comp_website."', '".$comp_phone."', '".$comp_addr1."', '".$comp_addr2."', 
		'".$comp_city."', '".$comp_state."', '".$comp_postcode."', '".$comp_country."', 
		'".$comp_created_by."', '".$comp_created_date."', '".$comp_modified_by."', '".$comp_modified_date."')";

		$this->db->query($sql);

		$this->db->trans_complete();
		//Transanction Complete
	 }

	/*************************************/
	/***Region***/
	/*************************************/

	 //Region Entry
	 public function region_entry($data){ 
		$region_name = $this->input->post("region_name");
		$region_enabled = 1;
		$region_created_by  = $_SESSION['username'];
		$region_created_date = date('Y-m-d H:i:s');
		$region_modified_by = $_SESSION['username'];
		$region_modified_date = date('Y-m-d H:i:s');

		//Trim
		$region_name = trim($region_name);

		//Transaction Start
		$this->db->trans_start();

		$sql = "insert into region_mst(region_name, region_enabled, region_created_by, region_created_date, region_modified_by, 
		region_modified_date) 
		values 
		('".$region_name."', '".$region_enabled."', '".$region_created_by."', '".$region_created_date."', '".$region_modified_by."', 
		'".$region_modified_date."')";

		$this->db->query($sql);

		$this->db->trans_complete();
		//Transanction Complete
	 }

	/*************************************/
	/***Product Categories***/
	/*************************************/

	//Product Category Entry
	public function prodcat_entry($data){ 
		$prodcat_name = $this->input->post("prodcat_name");
		$prodcat_enabled = 1;
		$prodcat_created_by  = $_SESSION['username'];
		$prodcat_created_date = date('Y-m-d H:i:s');
		$prodcat_modified_by = $_SESSION['username'];
		$prodcat_modified_date = date('Y-m-d H:i:s');

		//Trim
		$prodcat_name = trim($prodcat_name);

		//Transaction Start
		$this->db->trans_start();

		$sql = "insert into prodcat_mst(prodcat_name, prodcat_enabled, prodcat_created_by, prodcat_created_date, prodcat_modified_by, 
		prodcat_modified_date) 
		values 
		('".$prodcat_name."', '".$prodcat_enabled."', '".$prodcat_created_by."', '".$prodcat_created_date."', '".$prodcat_modified_by."', 
		'".$prodcat_modified_date."')";

		$this->db->query($sql);

		$this->db->trans_complete();
		//Transanction Complete
	 }
	 
   }  
?>